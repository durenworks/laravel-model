<?php namespace Durenworks;

class Model extends Illuminate\Database\Eloquent\Model {
	protected $validateRules = [];

	protected function validate(string $rule, array $input)
	{
		if (!isset($this->validateRules[$rule])) {
			$err = new ValidationNotFoundException();
			$err->setValidation(get_class($this), $rule);
			throw $err;
		}

		$validator = Validator::make(
			$input,
			$validateRules[$rule]
		);

		if ($validator->fails())
			return $validator->messages();

		return true;
	}

	public function __call(string $method, array $parameters)
	{
		$word = 'validateFor';
		if (starts_with($method, $word)) {
			$rule = substr($method, strlen($word));
			$rule = snake_case($rule);
			array_unshift($parameters, $rule);
			return call_user_func_array([$this, 'validate'], $parameters);
		}

		return super::__call($method, $parameters);
	}
}
