<?php namespace Durenworks;

class ValidationNotFoundException extends \RuntimeException {

	protected $rule;
	protected $model;

	/**
	 * Set the affected Eloquent model.
	 *
	 * @param  string   $model
	 * @return $this
	 */
	public function setValidation($model, $rule)
	{
		$this->model = $model;
		$this->rule = $rule;

		$this->message = "No model validation rule found for [{$model}::{$rule}].";

		return $this;
	}

	/**
	 * Get the affected Eloquent model.
	 *
	 * @return string
	 */
	public function getValidation()
	{
		return [$this->model, $this->rule];
	}

}
